vim.wo.number = true -- Set line number
vim.o.syntax = 'on'

vim.o.expandtab = true -- always uses spaces instead of tab characters (et).
vim.o.tabstop = 2 -- size of a hard tabstop (ts).
vim.o.shiftwidth = 2 -- size of an indentation (sw).
vim.o.softtabstop = 2 -- number of spaces a <Tab> counts for. When 0, feature is off (sts).
vim.o.autoindent = true
vim.o.smartindent = true

vim.o.cursorline = true

-- Lazy.nvim package manager for neovim extensions
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Using lazy package manager to install
require("lazy").setup({
  -- File manager
  {
    "nvim-tree/nvim-tree.lua",
    version = "*",
    lazy = false,
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
  },

  -- Nvim Theme
  { "catppuccin/nvim", name = "catppuccin", priority = 1000 },

  -- Server language config
  { "neovim/nvim-lspconfig" },

  -- Better highlight
  { "nvim-treesitter/nvim-treesitter" },

  -- Git highlight file and line changes
  { "lewis6991/gitsigns.nvim" },

  -- Status line
  { "nvim-lualine/lualine.nvim" },

  -- Tab mode for viewing files
  { "akinsho/bufferline.nvim", version = "*", dependencies = "nvim-tree/nvim-web-devicons" }
})


-- My keymap
vim.keymap.set("n", "<C-n>", function()
  vim.api.nvim_command(":NvimTreeToggle")
end)
